import { browser, by, element } from 'protractor';

export class AppPage {
  public navigateTo() {
    return browser.get('/');
  }

  public getCanvas() {
    return element(by.css('app-root canvas'));
  }
}
