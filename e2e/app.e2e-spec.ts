import { AppPage } from './app.po';

describe('draw-chart App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should init canvas', () => {
    page.navigateTo();
    expect(page.getCanvas().isPresent()).toEqual(true);
  });
});
