# DrawChart

The project is representation of the (Japanese) candle chart. The chart has been done with the 
usage of Angular 5 + html 5 canvas. The chart is flexible by width. The max width is 800 pixels. If 
less then data on the chart is adjustable to width.

## Parameters

Parameters are presented and set in chart component. Also default options are set in candle chart 
component if they were not passed to it component.

 - textStyle: string; // canvas text style
 - valuesPerTimeLine: number; // the number of (candle) bars shown per grid timeline
 - padding: number; // padding from border to drawing area inside canvas
 - stepWidth: number; // the width of the (candle) bar
 - stepPadding: number; // padding between (candle) bars
 - greedTextPadding: number; // padding between text and the border of a canvas
 - height: number; // the height of a canvas
 - volumeChartHeight: number; // the height of bottom volumes chart

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

