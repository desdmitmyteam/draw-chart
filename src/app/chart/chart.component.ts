import { Component, OnInit } from '@angular/core';
import { ChartService, ICurrencyTradeInfo } from './chart.service';

export interface IChartOptions {
  textStyle?: string; // canvas text style
  valuesPerTimeLine?: number; // the number of (candle) bars shown per grid timeline
  padding?: number; // padding from border to drawing area inside canvas
  stepWidth?: number; // the width of the (candle) bar
  stepPadding?: number; // padding between (candle) bars
  greedTextPadding?: number; // padding between text and the border of a canvas
  height?: number; // the height of a canvas
  volumeChartHeight?: number; // the height of bottom volumes chart
}

@Component({
  selector: 'app-chart',
  template: `<app-candle-chart [data]="data" [options]="options" #chartCanvas></app-candle-chart>`
})
export class ChartComponent implements OnInit {

  public data: ICurrencyTradeInfo[];
  public options = {
    greedStep: 80
  };

  public constructor(private chartService: ChartService) { }

  public ngOnInit() {
    this.chartService.get().subscribe((data) => this.data = data);
  }
}
