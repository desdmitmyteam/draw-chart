import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartComponent } from './chart.component';
import { ChartService } from './chart.service';
import { HttpClientModule } from '@angular/common/http';
import { CandleChartComponent } from './candle-chart/candle-chart.component';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule
  ],
  declarations: [
    ChartComponent,
    CandleChartComponent
  ],
  providers: [
    ChartService
  ],
  exports: [
    ChartComponent
  ]
})
export class ChartModule { }
