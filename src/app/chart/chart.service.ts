import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';

export interface ICurrencyTradeInfo {
  date: Date;
  close: number;
  open: number;
  high: number;
  low: number;
  volume: number;
}

export interface ICurrencyTradeServerInfo {
  t: number[]; // timeStamp
  c: number[]; // close price
  o: number[]; // open price
  h: number[]; // high price
  l: number[]; // low price
  v: number[]; // trading volume
}

@Injectable()
export class ChartService {

  public constructor(private http: HttpClient) {
  }

  public get() {
    return this.http
      .get<ICurrencyTradeServerInfo>('./assets/data/data.json')
      .map<ICurrencyTradeServerInfo, ICurrencyTradeInfo[]>((item) =>
        item.t.map((timestamp, index) => ({
          date: new Date(timestamp * 1000),
          close: item.c[index],
          open: item.o[index],
          high: item.h[index],
          low: item.l[index],
          volume: item.v[index]
        })));
  }

}
