import {
  AfterViewInit,
  Component,
  ElementRef,
  Input,
  OnChanges,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import { ChartService, ICurrencyTradeInfo } from '../chart.service';
import { CanvasService } from '../../common/canvas.service';
import { DatePipe } from '@angular/common';
import { Colors } from '../../common/const';
import { IChartOptions } from '../chart.component';

@Component({
  selector: 'app-candle-chart',
  templateUrl: './candle-chart.component.html',
  styleUrls: ['./candle-chart.component.css']
})
export class CandleChartComponent implements OnChanges, AfterViewInit {

  @Input() public data: ICurrencyTradeInfo[];

  @Input()
  public set options(value) {
    const defaultOptions: IChartOptions = {
      textStyle: '12px Arial',
      valuesPerTimeLine: 9,
      padding: 15,
      stepWidth: 8,
      stepPadding: 1,
      greedTextPadding: 5,
      height: 400,
      volumeChartHeight: 80
    };

    this._options = Object.assign({}, defaultOptions, value);
  }

  public get options() {
    return this._options;
  }

  @ViewChild('container') public container: ElementRef;
  @ViewChild('chartCanvas') public canvasElement: ElementRef;
  public canvas: HTMLCanvasElement;
  public ctx: CanvasRenderingContext2D;

  private _options: IChartOptions;

  public constructor(private chartService: ChartService, private canvasService: CanvasService) {
  }

  public ngOnChanges(changes: SimpleChanges): void {
    this.initChart();
  }

  public ngAfterViewInit(): void {
    this.canvas = this.canvasElement.nativeElement as HTMLCanvasElement;
    this.initChart();
  }

  public onResize() {
    if (this.canvas.width !== this.container.nativeElement.offsetWidth) {
      this.initChart();
    }
  }

  private initChart() {
    if (this.canvas && this.data && this.options) {
      this.canvas.height = this.options.height;
      this.canvas.width = this.container.nativeElement.offsetWidth;
      this.ctx = this.canvas.getContext('2d');

      this.drawGrid(this.canvas.width);
      this.drawChart(this.canvas.width);
    }
  }

  private drawGrid(width: number) {
    const datePipe = new DatePipe('en-US');
    const timeLineWidth = this.options.stepWidth * this.options.valuesPerTimeLine;
    const timeLinesCount = (width - this.options.padding * 2) / timeLineWidth;

    for (let i = 1; i < timeLinesCount; i++) {
      const itemNumber = i * this.options.valuesPerTimeLine - 1;
      const startX = i * timeLineWidth - (this.options.stepWidth / 2) + this.options.padding;

      this.canvasService.drawLine({
        ctx: this.ctx,
        startX,
        startY: this.options.padding,
        endX: startX,
        endY: this.canvas.height - this.options.padding,
        color: Colors.LigtBlue});

      this.canvasService.drawText({
        ctx: this.ctx,
        startX: startX - 15,
        startY: this.options.height - this.options.greedTextPadding,
        text: datePipe.transform(this.data[itemNumber].date, 'HH:mm'),
        textStyle: this.options.textStyle,
        color: Colors.Blue});
    }
  }

  private drawChart(width: number) {
    const valuesCount = (width - this.options.padding * 2) / this.options.stepWidth;
    const values = this.data.slice(0, valuesCount);
    const maxVolume = Math.max.apply(null, values.map((item) => item.volume));
    const maxPeak = Math.max.apply(null, values.map((item) => item.high));
    const minPeak = Math.min.apply(null, values.map((item) => item.low));
    const drawingAreaHeight = this.options.height - (this.options.padding * 4);
    const volumeMultiplier = this.options.volumeChartHeight / maxVolume;
    const candleMultiplier = drawingAreaHeight / (maxPeak - minPeak);

    for (let i = 0; i < (valuesCount - 1); i++) {
      const isPositive = this.data[i].close - this.data[i].open > 0;
      this.drawVolume(i, volumeMultiplier, drawingAreaHeight, isPositive);
      this.drawCandle(i, candleMultiplier, drawingAreaHeight, minPeak, isPositive);
    }
  }

  private drawVolume(
      index: number,
      multiplier: number,
      drowingAreaHeight: number,
      isPositive: boolean) {

    const height = multiplier * this.data[index].volume;
    this.canvasService.drawBar({
      ctx: this.ctx,
      upperLeftX: (index * this.options.stepWidth) + this.options.stepPadding + this.options.padding,
      upperLeftY: drowingAreaHeight - height + (this.options.padding * 2),
      width: this.options.stepWidth - (this.options.stepPadding * 2),
      height,
      color: isPositive ? Colors.LightGreen : Colors.LightRed
    });
  }

  private drawCandle(
      index: number,
      multiplier: number,
      drawingAreaHeight: number,
      minPeak: number,
      isPositive: boolean) {
    const step = ((index + 1) * this.options.stepWidth) - (this.options.stepWidth / 2);
    const upperLeftYValue = isPositive ? this.data[index].close : this.data[index].open;

    this.canvasService.drawLine({
      ctx: this.ctx,
      startX: step + this.options.padding,
      startY: drawingAreaHeight - (multiplier * (this.data[index].high - minPeak)) + (this.options.padding * 2),
      endX: step + this.options.padding,
      endY: drawingAreaHeight - (multiplier * (this.data[index].low - minPeak)) + (this.options.padding * 2),
      color: isPositive ? Colors.Green : Colors.Red});

    this.canvasService.drawBar({
      ctx:  this.ctx,
      upperLeftX: (index * this.options.stepWidth) + this.options.stepPadding + this.options.padding,
      upperLeftY: drawingAreaHeight - (multiplier * (upperLeftYValue - minPeak)) + (this.options.padding * 2),
      width: this.options.stepWidth - (this.options.stepPadding * 2),
      height: multiplier * Math.abs(this.data[index].open - this.data[index].close),
      color: isPositive ? Colors.Green : Colors.Red
    });
  }
}
