import { Injectable } from '@angular/core';

export interface ICanvasLine {
  ctx: CanvasRenderingContext2D;
  startX: number;
  startY: number;
  endX: number;
  endY: number;
  color: string;
}

export interface ICanvasText {
  ctx: CanvasRenderingContext2D;
  startX: number;
  startY: number;
  text: string;
  textStyle: string;
  color: string;
}

export interface ICanvasBar {
  ctx: CanvasRenderingContext2D;
  upperLeftX: number;
  upperLeftY: number;
  width: number;
  height: number;
  color: string;
}

@Injectable()
export class CanvasService {

  public drawLine({ctx, startX, startY, endX, endY, color}: ICanvasLine) {
    ctx.save();
    ctx.translate(0.5, 0.5);
    ctx.lineWidth = 1;
    ctx.strokeStyle = color;
    ctx.beginPath();
    ctx.moveTo(startX, startY);
    ctx.lineTo(endX, endY);
    ctx.stroke();
    ctx.restore();
  }

  public drawText({ctx, startX, startY, text, textStyle, color}: ICanvasText) {
    ctx.save();
    ctx.translate(0.5, 0.5);
    ctx.fillStyle = color;
    ctx.font = textStyle;
    ctx.fillText(text, startX, startY);
    ctx.restore();
  }

  public drawBar({ctx, upperLeftX, upperLeftY, width, height, color}: ICanvasBar) {
    ctx.save();
    ctx.translate(0.5, 0.5);
    ctx.fillStyle = color;
    ctx.fillRect(upperLeftX, upperLeftY, width, height);
    ctx.restore();
  }

}
