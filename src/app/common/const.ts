export enum Colors {
  Red = '#E76339',
  LightRed = '#D69D8E',
  Green = '#61A45C',
  LightGreen = '#99C7A9',
  Blue = '#7DB1ED',
  LigtBlue = '#C5DEF7'
}
