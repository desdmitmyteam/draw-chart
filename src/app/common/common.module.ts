import { NgModule } from '@angular/core';
import { CanvasService } from '../common/canvas.service';

@NgModule({
  providers: [
    CanvasService
  ]
})
export class CommonAppModule { }
