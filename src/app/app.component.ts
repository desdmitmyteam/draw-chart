import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `<app-chart></app-chart>`
})
export class AppComponent {
}
